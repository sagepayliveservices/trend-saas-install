#############################
#### Trend Agent Install ####
#############################

This Script is used to install the Trend agent, and register against the SagePay SaaS tenant. Use policy:id to automatically enroll the new agent to a predefined policy. 

# You need to Set the following env Vars for this script to work:

     $PROXY_HOST
     $PROXY_PORT
     $POLICY_ID
     $TENANT_ID
     $TOKEN
     

     example:

     PROXY_HOST=proxy.internal.env.domain.com
     PROXY_PORT=[Port]
     NO_PROXY=localhost
     POLICY_ID="policyid:XXX"
     TENANT_ID="[TenantID]"
     TOKEN="[TOKEN]"